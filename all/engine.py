import RPi.GPIO as GPIO
import time#from(https://pythonprogramming.net/gpio-motor-control-raspberry-pi/)

class engine:

	def __init__(self):

		GPIO.setmode(GPIO.BCM)
		
		self.leftengineforward = 7

		self.leftenginebackwards = 8
		
		self.rightengineforward = 9

		self.rightenginebackwards = 10

		GPIO.setup(self.leftengineforward,GPIO.OUT)
	
		GPIO.setup(self.leftenginebackwards,GPIO.OUT)
		
		GPIO.setup(self.rightengineforward,GPIO.OUT)
		
		GPIO.setup(self.rightenginebackwards,GPIO.OUT)

	def forward(self,movementtime):

		GPIO.output(self.leftengineforward,True)

		GPIO.output(self.leftenginebackwards,False)

		GPIO.output(self.rightengineforward,True)

		GPIO.output(self.rightenginebackwards,False)

		time.sleep(movementtime)

	def backwards(self,movementtime):

		GPIO.output(self.leftengineforward,False)

		GPIO.output(self.leftenginebackwards,True)

		GPIO.output(self.rightengineforward,False)

		GPIO.output(self.rightenginebackwards,True)

		time.sleep(movmenttime)
	
	def right(self, movementtime):

		GPIO.setup(self.leftengineforward, True)

		GPIO.setup(self.leftenginebackwards, False)

		GPIO.output(self.rightengineforward,False)

		GPIO.setup(self.rightenginebackwards, False)

		time.sleep(movmenttime)

	def left(self, movementtime):


		GPIO.setup(self.leftengineforward, False)

		GPIO.setup(self.leftenginebackwards, False)

		GPIO.setup(self.rightengineforward, True)

		GPIO.setup(self.rightenginebackwards, False)

		time.sleep(movmenttime)
	def leftright(self, movmenttime):
		

GPIO.cleanup()

