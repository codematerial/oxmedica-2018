#####################################################################################################
# Basic data types, casting, input and printing
#####################################################################################################

#print to the command line
print("Hello World")


#define a string, int and float
name = "John Smith"
age = 14
height = 1.8


#String manipulation
print("Hello %s, your age is %i and your height is %fm" % (name, age, height))


#User input
age = raw_input("Please enter your age\n") # Remeber this returns a STRING


#Cast to an int
age = int(age)

#Casting as floats and strings
ageAsFloat = float(age)
heightAsString = str(height)


#####################################################################################################
# Lists and Dictionaries
#####################################################################################################

#Define a list
favoriteFilms = ["The Avengers", "The Incredibles", "Wall-e"]


#Access a list:
firstFilm = favoriteFilms[0]
lastFilm = favoriteFilms[-1]


#Define a dictionary made of keys and values
tutorInfo = {"name":"sam", "age":23, "height":1.8, "favoriteFilms":favoriteFilms}


#Access a dictionary
#Here we're using a comma inside a print statements like this print(a,b,c,d), it's good for printing lots of things at once
print("Your tutors name is ", tutorInfo["name"])
print("Your tutors favorite films are", tutorInfo["favoriteFilms"])


#####################################################################################################
# Looping
#####################################################################################################

#Loop through a dictionary:
for key in tutorInfo:
	print(key, tutorInfo[key])


#Loop through a string:
for char in "sam":
	print(char)

#Loop through a list
for film in favoriteFilms:
	print(film)


#Loop a set number of times
for i in range(100):
	print(i)


#Loop infinitly
while True:
	print("Forever")


#####################################################################################################
# If statements
#####################################################################################################

#Compaire a variable to a string using ==
if name == "John Smith":
	print("I know you John")
else:
	print("Sorry I don't know who you are")


#You can also use > < =< => != as well as ==
if age > 20:
	print("You're old!")
else:
	print("You're not old")


#The function len() gets the length of a list, dictionary or string
#Don't forget we can use elif to ask a second question if the first question is false
if len(favoriteFilms) < 3:
	print("You need to watch more films") 

elif len(favoriteFilms) < 5:
	print("You have some of favorite films.")

else:
	print("You have lots of favorite films!")




#####################################################################################################
# Defining a function, don't forget to indent with tabs!
#####################################################################################################

#Define a basic function with one argument
def sayHello(name):
	print("Hello %s!" % name)

#calling the function
sayHello("sam")

#Define a function with two arguments and a return statement
def add(a,b):
	print("adding ", a, b)
	return a + b

#calling the function and saving the output to a
a = add(1, 2)
print("adding 1 and two is %i" % a)


#####################################################################################################
# Defining a class
#####################################################################################################

class WaterBottle:

	#Initialisation statement, don't forget the self!
	def __init__(self):
		self.weight = 500
		self.lidOn = True

	#Even a function with no arguments needs self!
	def takeLidOff(self):
		self.lidOn = False

	#Define a function of waterbottle with one argument
	def drink(self, amount):

		if self.lidOn:
			print("you cannot drink with the lid on")
		else:
			print("drinking!")
			self.weight = self.weight - amount


#####################################################################################################
# Calling a class
#####################################################################################################

#Create a new bottle
bottle = WaterBottle()

#Access one of the classes attributes using .
print("The weight of the bottle is %i" % bottle.weight)

#This will not change the weight as we haven't taken the lid off
bottle.drink(10)


bottle.takeLidOff()

#We can now drink from the bottle!
bottle.drink(10)

print("The weight of the bottle after drinking is %i" % bottle.weight)

#####################################################################################################
# Import statements
#####################################################################################################

#Importing a module
import time


#Using an abbreviation
import turtle as t

#calling a function of a module
t.forward()


#only importing a single function
from math import sqrt
print("The square root of 10 is %f" % sqrt(10))


#####################################################################################################
# Reading a file
#####################################################################################################

#Open a file filename.py using the open and read functions
#This will return the file as one long string
a = open("filename.py","r").read()
