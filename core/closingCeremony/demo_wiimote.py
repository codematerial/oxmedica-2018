#Fahad Albawardi

import cwiid

from time import sleep

from team_motors import Motors
import RPi.GPIO as GPIO
from subprocess import call

def ack(msg):
	print(msg)
	m.moveForward()
	sleep(0.2)
	m.moveBackward()
	sleep(0.2)
	m.stopMotors()


def stream():
	ack("streaming")
	call("raspivid -o - -t 0 -hf -vf -w 640 -h 480 -fps 24 -n | cvlc -vvv stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8160}' :demux=h264 &", shell=True)
	ack("streaming live")

def reboot():
	ack("rebooting")
	GPIO.cleanup()
	call("sudo reboot -h now", shell=True)


def shutdown():
	ack("shutting down")
	GPIO.cleanup()
	call("sudo shutdown -h now", shell=True)


m = Motors()
sleep(1)


try:
	ack("connecting")
	wii = cwiid.Wiimote()
	ack("connected")
except:
	m.rotateClockwise()
	sleep(0.5)
	m.rotateCounterClockwise()
	sleep(0.5)
	m.stopMotors()


wii.rpt_mode = cwiid.RPT_BTN


GPIO.setwarnings(False)


while True:
	
	buttons = wii.state['buttons']

	if buttons & cwiid.BTN_PLUS:
		reboot()

	if buttons & cwiid.BTN_HOME:
		quit()


	if buttons & cwiid.BTN_A:
		stream()


	if buttons & cwiid.BTN_MINUS:
		shutdown()


	elif buttons & cwiid.BTN_LEFT:
		m.moveBackward()


	elif buttons & cwiid.BTN_RIGHT:
		m.moveForward()


	elif buttons & cwiid.BTN_UP:
		m.moveLeft()


	elif buttons & cwiid.BTN_DOWN:
		m.moveRight()


	elif buttons & cwiid.BTN_1:
		m.rotateClockwise()


	elif buttons & cwiid.BTN_2:
		m.rotateCounterClockwise()

	else:
		m.stopMotors()

GPIO.cleanup()