#####################################################################################################
# GPIO setup
#####################################################################################################


#Importing the GPIO class from a library
import RPi.GPIO as GPIO

#Setting the board mode !IMPORTANT
GPIO.setmode(GPIO.BCM)


#Clean up at the end of your program (I'm not too sure what this does but i know it's important)
GPIO.cleanup()

#####################################################################################################
# GPIO input
#####################################################################################################

# Set pin 25 as an input so we can read its value
GPIO.setup(25, GPIO.IN)

#Get the state of pin 25
if GPIO.input(25):
	print("pin 25 is high")
else:
	print("pin 25 is low")


#####################################################################################################
# GPIO output
#####################################################################################################

#Set the pin as an output
GPIO.setup(7, GPIO.OUT)

#Set GPIO pin 7 as high
GPIO.output(7, True)

#Set GPIO pin 8 as low
GPIO.output(7, False)


#####################################################################################################
# Motor Pins
#####################################################################################################

pins = {"left_backwards":7, "left_forwards":8, "right_backwards":9, "right_forwards":10}