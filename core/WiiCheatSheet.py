#####################################################################################################
# Wii Setup
#####################################################################################################

import cwiid
import time

print("Press 1 + 2 on your Wii Remote now ...")

#sleep here because reasons
time.sleep(1)

#instanciate a wii remote OBJECT
wii = cwiid.Wiimote()

#set the reporting mode
wii.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC

#####################################################################################################
# Reading data
#####################################################################################################

#Looky here, wii.state is a dictionary
#print it out to see everything you can read from the controller
buttons = wii.state['buttons']
accelerometer = wii.state['acc']


#####################################################################################################
# Button press interpretation
#####################################################################################################

if buttons & cwiid.BTN_HOME:
	print("Home button has been pressed")


#####################################################################################################
# All other buttons
#####################################################################################################
#See if you can work out the buttons on the nunchuck

cwiid.BTN_HOME
cwiid.BTN_LEFT
cwiid.BTN_RIGHT
cwiid.BTN_UP
cwiid.BTN_DOWN
cwiid.BTN_1
cwiid.BTN_2
cwiid.BTN_A
cwiid.BTN_B
cwiid.BTN_HOME
cwiid.BTN_MINUS
cwiid.BTN_PLUS