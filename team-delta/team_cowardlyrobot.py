#import team_wiimote
import numpy
from tensorflow import keras
from camera import Camera
from pinet import PiNet
from team_motors import Motors

m = Motors()


extractor = PiNet()

classifier = keras.models.load_model("net2.h5")

camera = Camera(training_mode=False)

while True:

	frame = camera.next_frame()

	features = extractor.features(frame)

	classes = classifier.predict(numpy.array([features]))[0]

	box = classes[0]

	notBox = classes [1]

	print(classes)

	if box > 0.6 and notBox < 0.6:

		print("I can see the white box")
		m.moveLeft(1)
		m.moveForward(1)

	else:

		print("I cannot see the white box")
		pass
