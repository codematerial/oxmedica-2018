class WaterBottle:

	def __init__(self):

		self.weight = 500
		self.lidOn = True

	def drink(self, time):
		if not self.lidOn:
			print("drinking some water!")
			self.weight = self.weight - time * 10

	def removeLid(self):
		self.lidOn = False

#bob = WaterBottle()
#bob.removeLid()
#bob.drink(40)
#print(bob.weight)
