from team_motors import Motors
import RPi.GPIO as GPIO
import time 

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

line_sensor_pin = 25
m = Motors()
GPIO.setup(line_sensor_pin, GPIO.IN)

try:
	while True:
		m.moveRight(0)
		while GPIO.input(line_sensor_pin):
			time.sleep(0.1)
		while not GPIO.input(line_sensor_pin):
			time.sleep(0.1)
		m.moveLeft(0)
		while GPIO.input(line_sensor_pin):
			time.sleep(0.1)
		while not GPIO.input(line_sensor_pin):
			time.sleep(0.1)
finally:
	GPIO.cleanup()

# Perfectly done! Should work flawlessly - Sam