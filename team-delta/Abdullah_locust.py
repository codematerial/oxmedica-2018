import RPi.GPIO as GPIO

from time import sleep

GPIO.setmode(GPIO.BCM)

GPIO.setup(7, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(9, GPIO.OUT)
GPIO.setup(10, GPIO.OUT)

#forward
GPIO.output(7, True)
GPIO.output(8, False)
GPIO.output(9, True)
GPIO.output(10, False)

sleep(1)

#backward
GPIO.output(7, False)
GPIO.output(8, True)
GPIO.output(9, False)
GPIO.output(10, True)

sleep(1)

#turnright
GPIO.output(7, True)
GPIO.output(8, False)
GPIO.output(9, False)
GPIO.output(10, True)

sleep(1)

#turnleft
GPIO.output(7, False)
GPIO.output(8, True)
GPIO.output(9, True)
GPIO.output(10, False)

sleep(1)

GPIO.cleanup()