import RPi.GPIO as GPIO 
import time
from team_motors import Motors


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)


line_sensor_pin = 25

GPIO.setup(line_sensor_pin, GPIO.IN)

m = Motors()

try:
  
    while True:
       
        if GPIO.input(line_sensor_pin):
            print('The sensor is seeing a black surface')
            m.moveForward(0)

        else:
            print('The sensor is seeing a white surface')
            m.moveRight(0)
        
except KeyboardInterrupt:
    GPIO.cleanup()


# Perfect! - Sam