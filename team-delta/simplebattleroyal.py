import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

from team_motors import Motors

m = Motors()

front_line_sensor_pin = 25
back_line_sensor_pin = 24

GPIO.setup(front_line_sensor_pin, GPIO.IN)

try:
	while True:
		if GPIO.input(front_line_sensor_pin):
			m.moveForward(0)
		else:
			rotateClockwise(0)
			time.sleep(2)
finally:
	m.stopMotors()
	GPIO.cleanup()
