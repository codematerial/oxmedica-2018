import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

left forward = 7
left backward = 8
right forward = 9
right backward = 10

GPIO.setup(7, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(9, GPIO.OUT)
GPIO.setup(10, GPIO.OUT)
#Forward
GPIO.output(7, True)
GPIO.output(8, False)
GPIO.output(9, True)
GPIO.output(10, False)

time.sleep(0.5)
#Backward
GPIO.output(7, False)
GPIO.output(8, True)
GPIO.output(9, False)
GPIO.output(10, True)

time.sleep(0.5)
#Left/Right
GPIO.output(7, False)
GPIO.output(8, True)
GPIO.output(9, True)
GPIO.output(10, False)

time.sleep(0.5)
#Right/Left
GPIO.output(7, True)
GPIO.output(8, False)
GPIO.output(9, False)
GPIO.output(10, True)

time.sleep(0.5)

GPIO.cleanup()