from time import sleep
from team_motors import Motors

m = Motors()

try:

	m.moveForward(1)

	m.moveRight(1)

	m.moveLeft(1)

	m.moveBackward(1)

	m.rotateClockwise(1)

	m.rotateCounterClockwise(1)

finally:

	m.cleanup()

# Nice usage of cleanup function - Sam