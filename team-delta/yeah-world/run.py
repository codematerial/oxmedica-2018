import numpy
from tensorflow import keras
from camera import Camera
from pinet import PiNet


extractor = PiNet()

classifier = keras.models.load_model("net.h5")

camera = Camera(training_mode=False)

while True:

    frame = camera.next_frame()
    
    features = extractor.features(frame)

    classes = classifier.predict(numpy.array([features]))[0]

    fullBottle = classes[0]

    emptyBottle = classes [1]

    if sum(classes) > .5:

	    if emptyBottle > fullBottle:

	    	print("The bottle is empty")

	    else:

	    	print("The bottle is full")