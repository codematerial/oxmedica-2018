class WaterBottle:
	def __init__(self):
		self.weight = 500
		self.lidOn = True

	def drink(self, time):
		if not self.lidOn:
			print("drinking some watter!")
			self.weight = self.weight - time * 10
		else:
			print("Water bottle lid is still on")

	def removelidOn(self):
		print("lid has been removed!")
		self.lidOn = False

bob = WaterBottle()
bob.removelidOn()
bob.drink(5)
print(bob.weight)

# perfectly done, why not customize it a little? - Sam