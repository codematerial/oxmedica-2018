#Fahad Albawardi

import cwiid

from time import sleep

from team_motors_2 import Motors
import RPi.GPIO as GPIO



sleep(1)

wii = cwiid.Wiimote()

wii.rpt_mode = cwiid.RPT_BTN

m = Motors()

GPIO.setwarnings(False)

#line_sensor_pin = 25

#GPIO.setup(line_sensor_pin, GPIO.IN)

while True:
	
	buttons = wii.state['buttons']

	if buttons & cwiid.BTN_HOME:
		sleep(3)
		break


	elif buttons & cwiid.BTN_LEFT:
		m.moveBackward(0)


	elif buttons & cwiid.BTN_RIGHT:
		m.moveForward(0)


	elif buttons & cwiid.BTN_UP:
		m.moveLeft(0)


	elif buttons & cwiid.BTN_DOWN:
		m.moveRight(0)


	elif buttons & cwiid.BTN_UP and cwiid.BTN_RIGHT:
		m.moveForwardRight(0)


	elif buttons & cwiid.BTN_UP and cwiid.BTN_LEFT:
		m.moveForwardLeft(0)


	elif buttons & cwiid.BTN_DOWN and cwiid.BTN_RIGHT:
		m.moveBackwardRight(0)


	elif buttons & cwiid.BTN_DOWN and cwiid.BTN_LEFT:
		m.moveBackwardLeft(0)


	elif buttons & cwiid.BTN_1:
		m.rotateClockwise(0)


	elif buttons & cwiid.BTN_2:
		m.rotateCounterClockwise(0)

	elif buttons & cwiid.BTN_MINUS:
		m.speedDown(10)


	elif buttons & cwiid.BTN_PLUS:
		m.speedUp(-10)



	#elif buttons & cwiid.BTN_A:

		#time.sleep(1)
		#print("Initiialising Line Follower")

		#while not buttons & cwiid.BTN_A:

			#m.moveRight(0)
			#while GPIO.input(line_sensor_pin):
				#time.sleep(0.1)					# Sleep time doesn't have to be this long.
			#while not GPIO.input(line_sensor_pin):
				#time.sleep(0.1)
				#m.moveLeft(0)
			#while GPIO.input(line_sensor_pin):
				#time.sleep(0.1)
			#while not GPIO.input(line_sensor_pin):
				#time.sleep(0.1)

	else:
		m.stopMotors()

GPIO.cleanup()


# Nicely merged! Only thing I would encourage is some kind of toggle instead of having a loop in a loop - Sam