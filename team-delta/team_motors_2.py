#Ibrahim Alkhayatt

import RPi.GPIO as GPIO
from time import sleep

class Motors:
	def __init__ (self):
		#GPIO setup
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(7, GPIO.OUT)
		GPIO.setup(8, GPIO.OUT)
		GPIO.setup(9, GPIO.OUT)
		GPIO.setup(10, GPIO.OUT)
		#Frequency: How many times to turn the pin on and off each second
		frequency = 20
		#Dutycycle: How long the pin stays on each cycle, as a percent
		dutyCycle = 30

		#Motor pins
		#Left Forward
		self.lF = 8
		#Left Backward
		self.lB = 7
		#Right Forward
		self.rF = 9
		#Right Backward
		self.rB = 10
		#PWM 
		pwmLF = GPIO.PWM(self.lF, frequency)
		pwmLB = GPIO.PWM(self.lB, frequency)
		pwmRF = GPIO.PWM(self.rF, frequency)
		pwmRB = GPIO.PWM(self.rB, frequency)

	def moveForward (self, movementTime):
		print("Moving forwards!")
		GPIO.output(self.lF, True)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, True)
		GPIO.output(self.rB, False)
	

	def moveBackward (self, movementTime):
		print("Moving backwards!")
		GPIO.output(self.lF, False)
		GPIO.output(self.lB, True)
		GPIO.output(self.rF, False)
		GPIO.output(self.rB, True)
	
		
	def moveLeft (self, movementTime):
		print("Going right!")
		GPIO.output(self.lF, True)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, False)
		GPIO.output(self.rB, False)

		
	def moveRight (self, movementTime):
		print("Going left!")
		GPIO.output(self.lF, False)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, True)
		GPIO.output(self.rB, False)
		

	def moveForwardRight(self, movementTime):
		print("Moving forwards and to the right!")
		GPIO.output(self.LF, True)
		frequency = 20
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, True)
		frequency = 40
		GPIO.output(self.RB, False)


	def moveForwardLeft(self, movementTime, frequency):
		print("Moving forwards and to the left!")
		GPIO.output(self.LF, True)
		frequency = 40
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, True)
		frequency = 20
		GPIO.output(self.RB, False)


	def moveBackwardRight(self, movementTime, frequency):
		print("Moving backwards and to the right!")
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, True)
		frequency = 20
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, True)
		frequency = 40


	def moveBackwardLeft(self, movementTime, frequency):
		print("Moving backwards and to the right!")
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, True)
		frequency = 40
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, True)
		frequency = 20


	def speedUp (self, frequency):
		print("Speeding up!")
		frequency -= 10


	def speedDown (self, frequency):
		print("Speeding down!")
		frequency += 10


	def rotateCounterClockwise (self, movementTime):
		print("Rotating clockwise!")
		GPIO.output(self.lF, True)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, False)
		GPIO.output(self.rB, True)


	def rotateClockwise (self, movementTime):
		print("Rotating counterclockwise!")
		GPIO.output(self.lF, False)
		GPIO.output(self.lB, True)
		GPIO.output(self.rF, True)
		GPIO.output(self.rB, False)


	def stopMotors (self):
		print("Stopping!")
		GPIO.output(self.lF, False)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, False)
		GPIO.output(self.rB, False)

	def cleanup (self):
		GPIO.cleanup()

# Perfect, don't forget to call cleanup function - Sam