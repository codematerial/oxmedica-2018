import copy
import sys
from random import randint, choice


def translate(attributes, attr):

	value = attributes[attr]

	v = int(value)
	#if v > 4:
		#print("value %r cannot be translated"%v)
		#return ""
	try:
		if attr == "bodySize":
			return ["very small", "small", "medium sized", "large", "very large"][v]

		elif attr == "furLength":
			return ["thin skinned", "short haired", "hairy", "furry", "very furry"][v]

		elif attr == "legCount":
			return str(v*2)

		elif attr == "teethSize":
			return ["no", "small", "regular", "large", "huge"][v]

		elif attr == "eyeCount": 
			return str(v*2)

		elif attr == "temperature":
			return ["freezing", "cold", "warm", "hot", "very hot"][v]

		elif attr in ["prey", "predators"]:
			return ["no", "a couple of", "some", "many","loads of"][v]
	except:
			return str(v)


class animal:

	def __init__(self, env):

		#print("creating a new animal")

		self.env = env

		self.attr = {	"bodySize":	randint(0,4), # very small, small, medium, large, very large
						"furLength":randint(0,4), # skin, short haired, hairy, furry, very furry
						"legCount":	randint(0,4), # 0, 2, 4, 6, 8
						"teethSize":randint(0,4), # no teeth, small teeth, regular teeth, large teeth, huge teeth
						"eyeCount": randint(0,4)} # 0, 2, 4, 6, 8

	def __repr__(self):
		a =  "I am a %s, %s animal with %s legs, %s eyes and %s teeth.\nI live in a %s" % (	translate(self.attr, "bodySize"),
																		translate(self.attr, "furLength"),
																		translate(self.attr, "legCount"),
																		translate(self.attr, "eyeCount"),
																		translate(self.attr, "teethSize"),
																		self.env)
		return a 

	def mutate(self):

		#print("mutating")

		attrToMutate = choice(self.attr.keys())

		self.attr[attrToMutate] += choice([-1,1])

		#self.attr[attrToMutate] %= 5




	def fitness(self):

		rightTemp = abs(self.attr["furLength"] - self.env.attr["temperature"])
		hunger = (self.env.attr["prey"] - self.attr["bodySize"])
		winRate = self.attr["teethSize"] + self.attr["bodySize"]*8
		hideRate = (4-self.attr["bodySize"])*self.env.attr["predators"] + self.attr["eyeCount"]
		
		#return hunger*winRate + hideRate + rightTemp
		return hunger * winRate * hideRate * rightTemp

	def copy(self):
		#print("copying")
		return copy.deepcopy(self)
	
	def breed(self, mate, numberOfChildren):
		#print("breeding")
		matedAttributes = animal(self.env)

		for attrName in self.attr.keys():
			matedAttributes.attr[attrName] = (self.attr[attrName] + mate.attr[attrName])/2.0

		children = []

		for i in range(numberOfChildren):
			child = matedAttributes.copy()
			child.mutate()
			children.append(child)

		return children

class environment:

	def __init__(self, temp, prey, predators):
		self.attr = {"temperature":temp,"prey":prey,"predators":predators}
		print("creating a %s"%self)
		

	def __repr__(self):
		return "%s environement with %s prey and %s predators" % (	translate(self.attr, "temperature"),
																	translate(self.attr, "prey"),
																	translate(self.attr, "predators"))


if __name__ == "__main__":

	_,temp, prey,predators = sys.argv

	env = environment(int(temp),int(prey),int(predators))

	parent1 = animal(env)
	parent2 = animal(env)


	gen = 0

	while True:

		children = parent1.breed(parent2,8)

		for child in children:
			if child.fitness() >= parent1.fitness():
				parent1 = child
			elif child.fitness() >= parent2.fitness():
				parent2 = child

		print("Generation %i\nScore: %f:\n%s"%(gen, parent1.fitness(), parent1))

		gen += 1

		raw_input()