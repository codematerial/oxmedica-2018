import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

from team_motors import Motors

m = Motors()

line_sensor_pin = 25


GPIO.setup(line_sensor_pin, GPIO.IN)
try:
	while True:
		if GPIO.input(line_sensor_pin):
			m.moveForward(0)
		else:
			m.moveBackward(0)
			time.sleep(1)
			m.rotateClockwise(0)
			time.sleep(1)
finally:
	m.stopMotors()
	GPIO.cleanup()
