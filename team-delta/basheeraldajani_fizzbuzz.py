#https://gist.github.com/jaysonrowe/1592775

#Here we're defining fizzbuzz
def fizzbuzz(n):

#Here we are saying any value that is divisible by both 3 and 5 will be replaced with "FizzBuzz"
    if n % 3 == 0 and n % 5 == 0:
        return 'FizzBuzz'
#Here we are saying that any value that is divisible by 3 will be replaced by "Fizz"
    elif n % 3 == 0:
        return 'Fizz'
#Here we are saying that any value that is divisible by 5 will be replaced by "Buzz"
    elif n % 5 == 0:
        return 'Buzz'
#Here we are saying anything that isn't divisble by either 3 or 5 will be returned as the number in the loop
    else:
        return str(n)
#Here we are adding a new line for every number. The join command is a string method which returns a string concatenated with the elements of an iterable (a sequence one after another). The xrange command creates the values as you need them in a loop with a special technique called yielding, delays the evaluation of an expression until its value is needed.
print "\n".join(fizzbuzz(n) for n in xrange(1, 101))

#Wow. I want to see you doing this in group work! - Sam

                                                                          
#                                                                  -+shdmNNNmmmddhyo+:-`                                                               
#                                                              `:smNMMMMMMMMMMMMMMMMMMNNmyo:`                                                          
#                                                            -smMMMMMMMMMMMMMMMMMMMMMMMMMMMMNy/`                                                       
#                                                         `/dNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmo`                                                     
#                                                        /dMMMmddhhhddddmmmmNNMMMMMMMMMMMMMMMMMMMmo`                                                   
#                                                      .hMMms-``````````````...-:+shNMMMMMMMMMMMMMMd:                                                  
#                                                     :mMd/`                       `./yNMMMMMMMMMMMMNs`                                                
#                                                    :NMs`                             .oNMMMMMMMMMMMMy                                                
#                                                   :NMh                                `mMMMMMMMMMMMMMo                                               
#                                                  .NMN.                                sMMMMMMMMMMMMMMN.                                              
#                                                  sMMo                                 NMMMMMMMMMMMMMMM/                                              
#                                                  NMm`                                 hMMMMMMMMMMMMMMM+                                              
#                                                 `MM+                                  .yNMMMMMMMMMMMMM+                                              
#                                                 -MN`   `````         .:/++++/:-.        :hMMMMMMMMMMMM/                                              
#                                                 -Mo.oyhddddds.       -hmmNNNNMNmh:`      `+NMMMMMMMMMM:                                              
#                                                 :d:mNmhyyyysoo.        .-::::::ohmy:`      -NMMMMMMMMM`                                              
#                                                 ://+--:ssdss:-+       `+/ysmd//:`.-+/       /MMMMMMMMd`                                              
#                                                 ::  +/.hhd:+yh-       .o/+sss-::..+::.       mMMMmo+/++:--`                                          
#                                                 :: `-. `::::++           .----.`   .-`       yMN+` .:-:o.:/                                          
#                                                 -/         ./                                oh. .:-   h/ s                                          
#                                                 .o        ./    `.                            ` -s++:  h-`o                                          
#                                                  s       -/      :::` +`                         :hMm` h.+.                                          
#                                                  s      /-       ``.  -m:                       -.`m+ `y-/                                           
#                                                  +.     yoo:o-`.:/mh``.o/-                     `y`-+: +-+                                            
#                                                  ::     o`   .-.  .---.  :-                   ` -::.`+:o                                             
#                                                  -+    :-  -///////:`     /-                 `d.   --.+                                              
#                                                  `s   `o .o/:///:::/++/    /-               `.+s-  `/:                                               
#                                                   s   +.-do++oossyhdhyhd/   +`              +d: `---                                                 
#                                                   /- .+-y:.-::--.`    .-oh+  o             ::y                                                       
#                                                    s s`o                  :: y            ::`y                                                       
#                                                    .+o`    -::::::::-   ```//-`:`       -/. o:                                                       
#                                                     :os` :/.         :::y/:::/-      .::`   h                                                        
#                                                      /:.+.            .+`-:::.`   .::.     :+                                                        
#                                                       -:                `      -::.        s.                                                        
#                                                        -o`                 `:::`           h                                                         
#                                                        :yy/`          `.--:-`              h:                                                        
#                                                       /+/ ss+/:----::-.`                   o`y`                                                      
#                                                     /y-+s  +`                             /- ho.                                                     
#                                                  .+s/` o+   :-                          .:-  + -:`                                                   
#                                               `/s+.    /+    .:-                      -:.    /   -:-                                                 
#                                             -:o/`      `h.ss/:.`--.                `-:`      :     ./o:.                                             
#                                         .-:s+`          sNMMMNNmhs+/-.          `--.`        /       :/.:--`                                         
#                                   `.--::.`.o           .dMMMMMMMMMMmy/:--`   `-:-`      `````o        --  `-:---.                                    
#                             ..--::-.``    o`          :/mMMMMMMMMMMMMNh/.---::-:/+syhdmmmmmmmm         --     ``.:::-.`                              
#                       `.-:::.```         +-         `/-/MMMMMMMMMMMMMMMMmhyshdNNMMMMMMMMMMMMMM`         .:`        ``.:/--`                          
#                   .--::.``              /:         -/` oMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM+.`        `:.            `.-:--`                      
#              `.-::-.`                 `/-        .:.   sMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm `--`        .:.              `.-::-.                  
#          `.-:-.`                     `o-       .:.     oMMMMMMMMMMMMNNmhyssydMMMMMMMMMMMMMMMMo   `:-         -:`                `..::-..`            
#      `-::..`                          .--.   .:.       /MMMMMMMNms:-.so     `:hMMMMMMMMMMMMMM-     .:-`       `:-`                   `..-::-.`       
#    .:-oo.                                //-:.         .MMMMmy+-`    ++       `/NNMMMMMMMMMMh        .//.      .o-                         `.-::`    
#   .+`  `::                              `-+.            oo+y:        o/         s-odNMMMMMMM:          ./+.  .:/.                            ./o:-   
#  `o      -+                           ./:`                 ./        +-        ./  `-ohmNNhh             `//+:`                             /:`  +-  
#  /.       /:                        `:-`                    o        -`        /-      `--`/               `/-                             -+     +  
#  -         :                        `                       :-                 +`          :                 ``                            /`     `  
#                                                              .                 .           -                                                         
#                                                                                            `                                                         
#                                                                                                                                                      
#                                                                                                                                                      
#                                       ---` `---    `.--.``   .--------      .----..`     `-----`   .-----.``                                         
#                                      `yyyo :yyy   :syyyyyo-  syyyyyyyy      /yyyyyyyo`   +yyyyy:   +yyyyyyys.                                        
#                                      `yyyy::yyy  .yyyy.yyyy  .-+yyyo--      +yyy/:yyy/   syysyyo   +yyy::yyy+                                        
#                                      .yyyyy/yyy  -yyyo syyy`   /yyy/        +yyy++yyy-  `yyy-yyy`  +yyy--yyyo                                        
#                                      .yyyyyyyyy  -yyy+ yyyy`   /yyy/        +yyyyyyyo`  :yys syy:  oyyy--yyyo                                        
#                                      .yyyoyyyyy  -yyyo yyyy`   /yyy/        +yyy:-yyyo  oyy+`oyyo  +yyy--yyyo                                        
#                                      `yyy-syyyy  .yyys yyyy    /yyy/        +yyy:-yyys  yyyyyyyyy` +yyy-:yyyo                                        
#                                      `yyy.-yyyy   oyyyoyyy/    /yyy/        /yyyssyyy+ -yyy/-/yyy- +yyyssyyy/                                        
#                                      `+++` /++/    ./++/:.     -+++-        -++++++/:` -+++. .+++- :++++++/:            