#Ibrahim Alkhayatt

import RPi.GPIO as GPIO
from time import sleep

class Motors:
	def __init__ (self):
		#GPIO setup
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(7, GPIO.OUT)
		GPIO.setup(8, GPIO.OUT)
		GPIO.setup(9, GPIO.OUT)
		GPIO.setup(10, GPIO.OUT)
		#Left Forward
		self.lF = 8
		#Left Backward
		self.lB = 7
		#Right Forward
		self.rF = 9
		#Right Backward
		self.rB = 10

	def moveForward (self, movementTime):
		print("Moving forwards!")
		GPIO.output(self.lF, True)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, True)
		GPIO.output(self.rB, False)
		#if movementTime:
			#sleep(movementTime)
			#self.stopMotors()

	def moveBackward (self, movementTime):
		print("Moving backwards!")
		GPIO.output(self.lF, False)
		GPIO.output(self.lB, True)
		GPIO.output(self.rF, False)
		GPIO.output(self.rB, True)
		#if movementTime:
			#sleep(movementTime)
			#self.stopMotors()
		
	def moveLeft (self, movementTime):
		print("Going right!")
		GPIO.output(self.lF, True)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, False)
		GPIO.output(self.rB, False)
		#if movementTime:
			#sleep(movementTime)
			#self.stopMotors()
		
	def moveRight (self, movementTime):
		print("Going left!")
		GPIO.output(self.lF, False)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, True)
		GPIO.output(self.rB, False)
		#if movementTime:
			#sleep(movementTime)
			#self.stopMotors()
		
	def rotateCounterClockwise (self, movementTime):
		print("Rotating clockwise!")
		GPIO.output(self.lF, True)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, False)
		GPIO.output(self.rB, True)
		#if movementTime:
			#sleep(movementTime)
			#self.stopMotors()

	def rotateClockwise (self, movementTime):
		print("Rotating counterclockwise!")
		GPIO.output(self.lF, False)
		GPIO.output(self.lB, True)
		GPIO.output(self.rF, True)
		GPIO.output(self.rB, False)
		#if movementTime:
			#sleep(movementTime)
			#self.stopMotors()

	def stopMotors (self):
		print("Stopping!")
		GPIO.output(self.lF, False)
		GPIO.output(self.lB, False)
		GPIO.output(self.rF, False)
		GPIO.output(self.rB, False)

	def cleanup (self):
		GPIO.cleanup()

# Perfect, don't forget to call cleanup function - Sam