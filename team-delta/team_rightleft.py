import RPi.GPIO as GPIO
from team_motors import Motors
m = Motors()
front_line_sensor_pin = 25
back_line_sensor_pin = 25

try:
	while True:
		if front_line_sensor_pin == False:
			m.moveRight(1)
			m.moveLeft(1)
		else:
			m.rotateClockwise(0)
		if back_line_sensor_pin == True:
			m.rotateCounterClockwise(0)
finally:
	m.stopMotors()
	GPIO.cleanup()



