import RPi.GPIO as GPIO

from time import sleep

GPIO.setmode(GPIO.BCM)

#Set the pin 7,8,9, and 10 as an output
GPIO.setup(7, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(9, GPIO.OUT)
GPIO.setup(10, GPIO.OUT)

#Set GPIO pin 7 and 9 as high (Make robot go forward)

GPIO.output(7, True)
GPIO.output(8, False)
GPIO.output(9, True)
GPIO.output(10, False)

sleep(2)

#Set GPIO pin 7 and 10 as high (Make robot go right)

GPIO.output(7, True)
GPIO.output(8, False)
GPIO.output(9, False)
GPIO.output(10, True)

sleep(2)

#Set GPIO pin 8 and 10 as high (Make robot go backward)
GPIO.output(7, False)
GPIO.output(8, True)
GPIO.output(9, False)
GPIO.output(10, True)

sleep(2)

#Set GPIO pin 8 and 9 as high (Make robot go left)

GPIO.output(7, False)
GPIO.output(8, True)
GPIO.output(9, True)
GPIO.output(10, False)

sleep(2)

GPIO.cleanup()
