import RPi.GPIO as GPIO
import time
from motors import Motors

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

right_line_sensor_pin = 25
left_line_sensor_pin = 24

GPIO.setup(right_line_sensor_pin, GPIO.IN)
GPIO.setup(left_line_sensor_pin, GPIO.IN)

m = Motors()

try:
	while True:
		if not GPIO.input(right_line_sensor_pin): # Not sure your if logic is correct here...
			print("righting")
			m.moveRight # Missing some brackets here - Sam
		elif:
		if not GPIO.input(left_line_sensor_pin): #You're missing some indentation at least - Sam
		print("lefting")
		m.moveLeft # Missing some brackets here too. remember this is a function! - Sam
		else:
			print("march")
			m.moveForward()
except KeyboardInterrupt:
	GPIO.cleanup() # Probably best to switch these two. - Sam
	m.stopmotors()

# Nice attempt - Sam