#############################################################
# Dojo Instructions
#############################################################

"""
Below are some unfinished functions.
To test if your functions work, simply run this file.

palindrome(sentence)

	This function should take one string argument
	It should return True or False if the string is the same forwards as it is backwards
	It should ignore punctuation and spaces


encrypt(message)

	This function should take one string argument
	It should return a string where each character is shifted by one. a->b, b->c, x->y, etc
	For example encrypt("banana") should return "cbobob"


decrypt(message)

	This should decrypt the encrypted message returned by encrypt
	For example decrypt("cbobob") should return "banana"


IMPORTANT TIPS

	message[::-1] will return the string message reversed
	a string is just a list of characters
	ord(char) will return the character char as a number
	chr(num) will return the integer num as a character
"""

#############################################################
# Functions
#############################################################

def palindrome(sentence):
	if "red rum, sir, is murder" is "red rum, sir, is murder":
		return True 
	
		 

		

def encrypt(message):
	return 

def decrypt(message):
	return 

"".replace("","")



#############################################################
# Testing code, do not edit
#############################################################

if __name__ == "__main__":

	#Palindrome testing
	print("Testing the palindrome function")

	palindromeTest = {"anna":True,"civic":True,"banana":False,"button":False,"red rum, sir, is murder!":True,"was it a cat i saw?":True}
	
	for word in palindromeTest:
		if palindrome(word) == palindromeTest[word]:
			print("\tTest %r passed" % word)
		else:
			print("\tTest %r failed" % word) 
			print("\t\tResult should have been %r"% palindromeTest[word])
			print("\t\tYour answer was %r\n" % palindrome(word))

	#Encryption testing
	print("\nTesting the encryption function")

	encryptionTest = {"civic":"djwjd","banana":"cbobob","button":"cvuupo","red rum, sir, is murder!":"sfe!svn-!tjs-!jt!nvsefs\"","was it a cat i saw?":"xbt!ju!b!dbu!j!tbx@"}

	for message in encryptionTest:
		if encryptionTest[message] == encrypt(message):
			print("\tTest %r passed" % message)
		else:
			print("\tTest %r failed" % message)
			print("\t\tResult should have been %r" % encryptionTest[message])
			print("\t\tYour answer was %r\n" % encrypt(message))

	print("\nTesting the decryption function")

	for message in encryptionTest:
		if message == decrypt(encryptionTest[message]):
			print("\tTest %r passed" % message)
		else:
			print("\tTest %r failed" % message)
			print("\t\tResult should have been %r" % encryptionTest[message])
			print("\t\tYour answer was %r\n" % encrypt(message))

