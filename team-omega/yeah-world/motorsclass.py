import RPi.GPIO as GPIO
import time


class engine:
	def __init__(self):
		GPIO.setmode(GPIO.BCM)
		self.LF = 7
		self.LB = 8
		self.RF = 9
		self.RB = 10


		GPIO.setup(self.LF, GPIO.OUT)
		GPIO.setup(self.LB, GPIO.OUT)
		GPIO.setup(self.RF, GPIO.OUT)
		GPIO.setup(self.RB, GPIO.OUT)



	def forward(self):
		GPIO.output(self.LF,True)
		GPIO.output(self.LB,False)
		GPIO.output(self.RF,True)
		GPIO.output(self.RB,False)


	def backwards(self):
		GPIO.output(self.LF,False)
		GPIO.output(self.LB,True)
		GPIO.output(self.RF,False)
		GPIO.output(self.RB,True)


	def right(self):
		GPIO.output(self.LF,True)
		GPIO.output(self.LB,False)
		GPIO.output(self.RF,False)
		GPIO.output(self.RB,True)

	def left(self):
		GPIO.output(self.LF,False)
		GPIO.output(self.LB,True)
		GPIO.output(self.RF,True)
		GPIO.output(self.RB,False)

	def stop(self):
		GPIO.output(self.LF,False)
		GPIO.output(self.LB,False)
		GPIO.output(self.RF,False)
		GPIO.output(self.RB,False)


GPIO.cleanup()


