import cwiid
import time
from motorsclass import engine
import RPi.GPIO as GPIO

print("press 1 + 2 on your remote to connect")

time.sleep(2)


wii = cwiid.Wiimote()

e = engine()

wii.rpt_mode = cwiid.RPT_BTN

try:
	while True:

		buttons = wii.state['buttons']

		if (buttons & cwiid.BTN_HOME):
			print("home button has been pressed")

		if (buttons & cwiid.BTN_LEFT):
			print("left button has been pressed")
			e.forward()
			
		if (buttons & cwiid.BTN_RIGHT):
			print("right button has been pressed")
			e.backwards()
		if (buttons & cwiid.BTN_UP):
			e.right()
			print("up button has been pressed")
		if (buttons & cwiid.BTN_DOWN):
			print("down button has been pressed")
			e.left()
		if (buttons & cwiid.BTN_1):
			print("number 1 button has been pressed")

		if (buttons & cwiid.BTN_2):
			print("number 2 button has been pressed")

		if (buttons & cwiid.BTN_A):
			print("A button has been pressed")

		if (buttons & cwiid.BTN_B):
			print("B button has been pressed")
			e.stop()
		if (buttons & cwiid.BTN_MINUS):
			print("minus button has been pressed")

		if (buttons & cwiid.BTN_PLUS):
			print("plus button has been pressed")

finally:
	GPIO.cleanup()


# Nicely done, but WHY ARE THERE SO MANY BLANK LINES??? XD - Sam
# somebody played with my code and put a code in line 200 that's why lmfao