for a in range(1, 101):

	if a%15 == 0:
		print ("FizzBuzz")

	elif a%3 == 0:
		print("Fizz")

	elif a%5 == 0:
		print("Buzz")

	else:
		print(a)

#Perfect! - Sam
#                                                 ````.-/++-                                                                                           
#                                             `.:+syhmmNNNNh.                                                                                          
#                                       .-/oyhmmNNMMMMMMMMMNh-`                                                                                        
#                                 `.-/+smNNMMMMMMMMMMMMMMMMMNms.                                                                                       
#                             `-/ohdNMMMMMMMMMMMMMMMMMMMMMMMMMNh.                                                                                      
#                        `./shmmNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNy`                                                                                     
#                     `./ymNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMo`                                                                                    
#                  .:symNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm:                                                                                    
#              `./ydNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNd`                                                                                   
#             `omNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNs.                                                                                  
#             `sNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh-                                                                                 
#              `:yNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNd-                                                                                
#                `/mNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNd:`                      ``..-.                                                  
#                  .yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm/               ..-:/syydmmNy`                                                 
#                   `oNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNMNNmd+.      ``.-/sydmNNMMMNMNNNy`                                                 
#                    `/mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNdhysooss``...+shmNNMMMMMMMMNNmds+.                                                  
#                      :dNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNmdyssoossyhmNNdhdmmNNMMMMMMMNmhy+:-.`                                                     
#                       -dNMMMMMMMMMMMMMMMMMMMMMMMMMMNNNmdysooosyhdmNMMMMMMMMMMNNNNNmdys/:-`                                                           
#                        .hMMMMMMMMMMMMMMMMMMMMMMNNmhyysoooyhmNNMMMMMMMMMMMMNNmdyo::``` `                                                              
#                         .smMMMMMMMMMMMMMMMNmdhysooosyhdmNMMMMMMMMMMNNNmdmmmho:`                                                                      
#                          `:dNMMMMMMMNNmmdysooosyhmNNMMMMMMMMMNNNmdhhyyoo+++++oo+:..`                                                                 
#                            .yNNNNmdhsoooosydmNMMMMMMMMMMNNmdhhys++/--``        `:ohy/`                                                               
#                            ``oyoooooshdmNNMMMMMMMMNNmddhyyso/--/- `             ```-/so`                                                             
#                            ``.yddmNNMMMMMMMNNNmmdhyyyyo+:.`  .NNd:.` `       ```-//  `sy-                                                            
#                       ``.:+ydNMMMMMMMMMMNmdhhyyyyy+:.`::      /dNmdhyyyyyyyyyyhhh+-   `sd`                                                           
#                  `.-/shNMMMNMMMMMMMNmhhhyyyyo+:.``````/m:      `:ohhhhy+--:/oooshmo    `h/                                                           
#              `./ydmNNMMMMMMMMNmhNmhyyyyyo:-` `-/++++/:sNy`        .sd+`-/o+++dNy/dh     .h.`                                                         
#           -shmNMMMMMMMMNNNdh+-.yNdyyyo:.` `-oyos+/--:+dmy.        `+h/o:. `  :+.`h+      hd:        .o+.                                             
#           :NMMMMNMMNdhhs:`    sMmhyy/````.ss-.+/:`...-/-hh-`      `:Nh``      ``ss`      -mo        ohdd                                             
#            yNMMmhs/-```      -mNyys` `  -h/`.//:.`..-/./-:h:`       /yy+-.` `/yho`:oydddhdNm/.`     `/dN`                                            
#            `+o:. `          .dmhyy-`.//od+..//:`.``-/./-..:d.        ``:+syyyo:.odNMMNNNNNNNNNmhsoosdNNs                                             
#                             -Nhyy/  .+osN:.+/:..`.::./....+m.      `.+yyyyyys-.dMMMNmo/.``-dhohmNNmdy+.`           /d++/////+/ss                     
#                             /Nhys. /ydmNN///:....::-/..`-/-m.    -odNNNMMNMMNmomMMMd.`    `ys``...````             od.       `.m:                    
#                             /Nhy/ `y:`./yd+:..../:-/.`.-/.ss`  `omMMMNdyyyNMMMdsddd+..`   `hs`                     od.        .m+                    
#                             :Nhy:  -s.  .oh:.../-:/...::-y+`  .dNMNms:````.ooo:````.+do`  -mo                      om.`   ````.m+                    
#                             /Nyy:   os   `-os++-::``./oso-    sNNNy-```:+. `-:++//: `+h.  sd:                      :No////+osssm+                    
#                             +Nyy:   +o     `.:oosooooo/.`    -NNNs`` `/dy`-yo//++:.  `y- `dd.                      `hdhhhhhhhhdm-                    
#                             /myy/   +s         `````` `/sy- -hNNo`   `odm:`.+ss+:.`   `` :ds                        `:hdhhhhdds-                     
#                             :Nyy/   -y.               -msho/dNNs`     `:os` ..`          yN/                          -smddyo:``                     
#                             -Ndyo`  `h:               `:sssss+-``       ``              `Ns`                          `oNN:`                         
#                             `yNhy-` `+/`                                               `sh.                          `oNMMd/                         
#                              -hdys.` /+`                                              .sm-                     ``:ososmmNNm-                         
#                               `omhs. +o`                                           `.+hs-                     `odmy+---:sMo`                         
#                                `/yhs/+s`                                         `-+dy-`                     .hNs.`     :M.                          
#                                  .ydyhh:.                                     `-oyhh+`                      `yNo`       :M.                          
#                                   `/sdNds+/-`                               ./ymNmh.                       `oN+`    .-:+yNs+:-`                      
#                                     `-sdddhhso/..`                    `..:+ydhm+ohN:`                     `yd/      .--------.`                      
#                                        .-/ohdmdhyso+++////////////+ssyhdmNho:.y` .smy+-.`                .sm:                                        
#                                           ``/ymNdhyyyydmd+////yms/+oNNdh/omh+-y- `-hmNmhys+:-`         `/dd:                                         
#                                              `-oyy....-:o+.   `:y-`oNdys``-hNhh/ +s-/do-:oydmds/.`   `-sd+.                                          
#                                                `-o+-`    +d`   `/dyNNhys.  yy.-y-./o+:h:  `.:oymdho+ohho-`                                           
#                                                  `oh+.   .ms///.`md//hdho`-mo``-y.``:ysy:`     .:+sys/.                                              
#                                                   `ods:`  ///sd. h/`..+mmyymm:..:s:``/d/d+`                                                          
#                                                    yNdy:    /s-  h/`.``dmys/om+.`-h+/s.`sNo`                                                         
#                                                   -dNNdy. ``oy:` yo`.`-Mhy:  +m+..-dh.  `ym-                                                         
#                                                  .dmsomh+`   -//--s.../Nhy`  `+N:``/y`   .hd.                                                        
#                                                 `sm:`.mdy:    `.+o+o..oNhy`   `ss.`.h+    -ds`                                                       
#                                                `+N/  `ydy+       +ym/`-mhy:   ``h-.`so     /m-                                                       
#                                                -mh    omy+        ./y/`+mhs.    y-.-y+     `ho                                                       
#                                                dm:    /Ny+          `s:.smys. ``hy//y-      -h:                                                      
#                                               +No     omyo-::/.`   ``:dyshdhyoys-``:h`       +y                                                      
#                                              .hd.```-+dNmdsyydmdosyys-`yy-``.:s:```o/        .h`                                                     
#                                              +M/:shddydmh+odddmdy+/-`   -+/-``++`.++`        `y:                                                     
#                                             `hNNmh/. .dds-m-``  `          ./oohoh/`         `sh`                                                    
#                                             `:/-`    /Nds-d`                   `-oy           +N-                                                    
#                                                      yNhs-s                      `-           .M+  