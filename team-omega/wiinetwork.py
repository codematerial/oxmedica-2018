import cwiid
import time
from motorsclass import engine
import RPi.GPIO as GPIO
import numpy
from tensorflow import keras
from camera import Camera
from pinet import PiNet

print("press 1 + 2 on your remote to connect")

time.sleep(2)
extractor = PiNet()
classifier = keras.models.load_model("net.h5")

wii = cwiid.Wiimote()

e = engine()

wii.rpt_mode = cwiid.RPT_BTN
camera = Camera(training_mode=False)

try:
	while True:
		frame = camera.next_frame()
	
		features = extractor.features(frame)
		classes = classifier.predict(numpy.array([features]))[0]
		buttons = wii.state['buttons']

		print(classes)

		if classes[0]>0.6 :
			e.backwards()
			time.sleep(2)
			e.right()
			time.sleep(2)
		elif (buttons & cwiid.BTN_HOME):
			print("home button has been pressed")

		elif (buttons & cwiid.BTN_LEFT):
			print("left button has been pressed")
			e.backwards()
			
		elif (buttons & cwiid.BTN_RIGHT):
			print("right button has been pressed")
			e.forward()
		elif (buttons & cwiid.BTN_UP):
			e.right()
			print("up button has been pressed")
		elif (buttons & cwiid.BTN_DOWN):
			print("down button has been pressed")
			e.left()
		elif (buttons & cwiid.BTN_1):
			print("number 1 button has been pressed")

		elif (buttons & cwiid.BTN_2):
			print("number 2 button has been pressed")

		elif (buttons & cwiid.BTN_A):
			print("A button has been pressed")

		elif (buttons & cwiid.BTN_B):
			print("B button has been pressed")
			e.stop()
		elif (buttons & cwiid.BTN_MINUS):
			print("minus button has been pressed")

		elif (buttons & cwiid.BTN_PLUS):
			print("plus button has been pressed")


finally:
	GPIO.cleanup()


# Nicely done, but WHY ARE THERE SO MANY BLANK LINES??? XD - Sam
