import RPi.GPIO as GPIO
import time
from motorsclass import engine
E = engine() 
GPIO.setmode(GPIO.BCM)

line_sensor_pin = 24
GPIO.setup(24, GPIO.IN)
try:
	while True:
		if GPIO.input(line_sensor_pin):
			print("The sensor is seeing a white surface!")
			E.right()

		else:
			print("the light sensor is seeing black surface!")
			E.forward()

finally:
	GPIO.cleanup() # Might be good to move this function into your engine class - Sam

# Nicely done! - Sam