import RPi.GPIO as GPIO  # Import the GPIO Library
import time  # Import the Time library

# Set the GPIO modes
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# Set variables for the GPIO pins
line_sensor_pin = 25

# Set pin 25 as an input so we can read its value
GPIO.setup(line_sensor_pin, GPIO.IN)

try:
    # Repeat the next indented block forever
    while True:
        # If the sensor is high (=1), it's above a white surface
        if GPIO.input(line_sensor_pin):
            print('The sensor is seeing a white surface')
        else:
            print('The sensor is seeing a black surface')
        time.sleep(0.2)

# If you press CTRL+C, cleanup and stop
except KeyboardInterrupt:
    GPIO.cleanup()