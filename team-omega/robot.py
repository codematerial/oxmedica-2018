import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)

LF = 7
LB = 8
RF = 9
RB = 10




GPIO.setup(LF, GPIO.OUT)
GPIO.setup(LB, GPIO.OUT)
GPIO.setup(RF, GPIO.OUT)
GPIO.setup(RB, GPIO.OUT)

sleep(1)

#motors going forwards

GPIO.output(LF,True)
GPIO.output(LB,False)
GPIO.output(RF,True)
GPIO.output(RB,False)

sleep(1)

#motors going backwards


GPIO.output(LF,False)
GPIO.output(LB,True)
GPIO.output(RF,False)
GPIO.output(RB,True)

sleep(1)

#motors turning left

GPIO.output(LF,False)
GPIO.output(LB,True)
GPIO.output(RF,True)
GPIO.output(RB,False)

sleep(1)

#motors turning right
GPIO.output(LF,True)
GPIO.output(LB,False)
GPIO.output(RF,False)
GPIO.output(RB,True)


GPIO.cleanup()





