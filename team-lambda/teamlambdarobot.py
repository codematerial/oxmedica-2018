import RPi.GPIO as GPIO
import time 
 

#Setting the board mode !IMPORTANT
GPIO.setmode(GPIO.BCM)
LF = 7
LB = 8
RF = 9
RB = 10

GPIO.setup(LF, GPIO.OUT)
GPIO.setup(LB, GPIO.OUT)
GPIO.setup(RF, GPIO.OUT)
GPIO.setup(RB, GPIO.OUT)

#Set GPIO pin 7 as high


#Set GPIO pin 8 as low
GPIO.output(LF, True)
GPIO.output(LB, False)
GPIO.output(RF, False)
GPIO.output(RB, True)

time.sleep(3)


GPIO.cleanup()
