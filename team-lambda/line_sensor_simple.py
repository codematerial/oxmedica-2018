import RPi.GPIO as GPIO
from motorclass import Motors
import time

m = Motors()

GPIO.setmode(GPIO.BCM)

line_sensor_pin = 25

GPIO.setup(line_sensor_pin, GPIO.IN)

try:
	while True:
		if GPIO.input(line_sensor_pin):
			print('The sensor is seeing a white surface')
			m.forward()
			time.sleep(0)

		else:
			print('The sensor is seeing a black surface')
			m.right()
			time.sleep(0)
			m.forward()
	
		
finally:
	m.stop()
	GPIO.cleanup()