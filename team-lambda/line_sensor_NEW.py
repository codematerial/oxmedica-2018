import RPi.GPIO as GPIO
from motorclass import Motors
import time

m = Motors()

GPIO.setmode(GPIO.BCM)

line_sensor_pin = 25

GPIO.setup(line_sensor_pin, GPIO.IN)

try:
	while True:
		if GPIO.input(line_sensor_pin):
			print('The sensor is seeing a white surface')
			m.forward()
			m.right() # Make sure this isn't turning on the spot or it'll just spin around! - Sam
        elif # The GPIO.input returns True or False! So can't detect 3 different states. Also you need a condition after an elif - Sam
        	print('The sensor is seeing a black surface')
			m.forward()
			m.left()
		else:
			print("The sensor doesn't see anything!!")
			m.stop()
except KeyboardInterrupt:
	GPIO.cleanup()