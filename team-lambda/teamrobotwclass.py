import RPi.GPIO as GPIO
import time 
from classmotorsteamlambda import Motors

m = Motors()

m.forward()

time.sleep(3.5)

m.rotate()

time.sleep(0.2)

m.backward()

time.sleep(1)

GPIO.cleanup()
