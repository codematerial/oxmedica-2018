import cwiid
import time
import numpy
from motorclass import Motors 
from tensorflow import keras
from camera import Camera
from pinet import PiNet

m = Motors()

extractor = PiNet()

classifier = keras.models.load_model("example/model.h5")

camera = Camera(training_mode=False)

wii = cwiid.Wiimote()

wii.rpt_mode = cwiid.RPT_BTN

while True:

	frame = camera.next_frame()

	features = extractor.features(frame)

	classes = classifier.predict(numpy.array([features]))[0]                      

	buttons = wii.state['buttons']

	if buttons & cwiid.BTN_LEFT:
		print("Left button has been pressed")
		m.left()
	elif buttons & cwiid.BTN_RIGHT:
		print("right button has been pressed")
		m.right()
	elif buttons & cwiid.BTN_DOWN:
		print("Down button has been pressed")
		m.backward()
	elif buttons & cwiid.BTN_UP:
		print("up button has been pressed")
		m.forward()
		if classes[0] >= 0.6 and classes[0] > classes[1]:
			print("an obstacle has been detected")
			m.stop()
			time.sleep(1)
			m.left()
			time.sleep(0.2)
			m.forward()
			time.sleep(0.2)
			print("the car passed the obstacle")
		else:
			m.forward()
	else:
		m.stop()


# Perfect and simple, only thing I would say is why all the blank lines below? - Sam









