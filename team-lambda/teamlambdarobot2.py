import RPi.GPIO as GPIO
import time 

#Setting the board mode !IMPORTANTs
GPIO.setmode(GPIO.BCM)
LF = 7
LB = 8
RF = 9
RB = 10

GPIO.setup(LF, GPIO.OUT)
GPIO.setup(LB, GPIO.OUT)
GPIO.setup(RF, GPIO.OUT)
GPIO.setup(RB, GPIO.OUT)

#Set GPIO pin 7 as high


#Set GPIO pin 8 as low
GPIO.output(LF, True)
GPIO.output(LB, False)
GPIO.output(RF, True)
GPIO.output(RB, False)

time.sleep(1.5)

GPIO.output(LF, False)
GPIO.output(LB, True)
GPIO.output(RF, False)
GPIO.output(RB, True)

time.sleep(1.5)

GPIO.output(LF, True)
GPIO.output(LB, False)
GPIO.output(RF, False)
GPIO.output(RB, True)

time.sleep(1.5)


GPIO.cleanup()
