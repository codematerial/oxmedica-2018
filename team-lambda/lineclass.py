import RPi.GPIO as GPIO
from motorclass import Motors
import time

m = Motors()

class line:

	GPIO.setmode(GPIO.BCM)
	self.line_sensor_pin = 25

	GPIO.setup(self.line_sensor_pin, GPIO.IN)

	def f-white(self):# move forward if white
		if GPIO.input(self.line_sensor_pin):
			print('The sensor is seeing a white surface')
			m.forward()

	def f-black(self):# move forward if black
		if not GPIO.input(self.line_sensor_pin):		
        	print('The sensor is seeing a black surface')
			m.forward()

	def b-white(self):# move backward if white
		if GPIO.input(self.line_sensor_pin):
			print('The sensor is seeing a white surface')
			m.backward()

	def b-black(self):# move backward if black
		if not GPIO.input(self.line_sensor_pin):		
        	print('The sensor is seeing a black surface')
			m.backward()

	def z-black(self):# move in zigzag 
		if GPIO.input(self.line_sensor_pin):
			print('The sensor is seeing a white surface')
			m.right()
		else:
        	print('The sensor is seeing a black surface')
			m.left()		

	