from motor_class import motor as oldMotor
from improved_motor_class import motor as newMotor
import time
import RPi.GPIO as G

om = oldMotor()
nm = newMotor()

front_lsp = 25
back_lsp = 24

G.setup(front_lsp, G.IN)
G.setup(back_lsp, G.IN)



G.setwarnings(False)
G.setmode(G.BCM)

try:
	while True:
		if G.input(front_lsp) and G.input(back_lsp):
			om.forward()
		elif G.input(front_lsp) and not G.input(back_lsp):
			nm.goRight(1)
			nm.goLeft(1)
		elif G.input(back_lsp) and not G.input(front_lsp):
			nm.backward(1)
			nm.turnRight(1.5)
		

		
finally:
	nm.stop()
	G.cleanup()	