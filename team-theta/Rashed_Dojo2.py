#############################################################
# Dojo Instructions
#############################################################

"""
Below are some unfinished functions.
To test if your functions work, simply run this file.

palindrome(sentence)

	This function should take one string argument
	It should return True or False if the string is the same forwards as it is backwards
	It should ignore punctuation and spaces


encrypt(message)

	This function should take one string argument
	It should return a string where each character is shifted by one. a->b, b->c, x->y, etc
	For example encrypt("banana") should return "cbobob"


decrypt(message)

	This should decrypt the encrypted message returned by encrypt
	For example decrypt("cbobob") should return "banana"


IMPORTANT TIPS

	message[::-1] will return the string message reversed
	a string is just a list of characters
	ord(char) will return the character char as a number
	chr(num) will return the integer num as a character
"""

#############################################################
# Functions
#############################################################

def palindrome(sentence):
	message = raw_input("Please enter something you want to check  \n")
	if message[::-1] == message:
		print("This sentence is the same forwards as it is backwards")
	else:
		print("Sadly, This sentence isn't the same forwards as it is backwards")
def encrypt(messageEn):
	encrypt.messageEn = raw_input("Please enter something you want to encrypt \n")
	messageEn = int(ord(char))
	messageEn = m + 1
	messageEn = str(chr(num))
	print encrypt.messageEn

def decrypt(messageDe):
	decrypt.messageDe = raw_input("Please enter something you want to decrypt \n")
	messageDe = int(ord(char))
	messageDe = m - 1
	messageDe = str(chr(num))
	print messageDe


#I can totally see what you're trying to do here!
#Seriously, well done, this is pretty impressive



#############################################################
# Testing code, do not edit
#############################################################

if __name__ == "__main__":

	#Palindrome testing
	print("Testing the palindrome function")

	palindromeTest = {"anna":True,"civic":True,"banana":False,"button":False,"red rum, sir, is murder!":True,"was it a cat i saw?":True}
	
	for word in palindromeTest:
		if palindrome(word) == palindromeTest[word]:
			print("\tTest %r passed" % word)
		else:
			print("\tTest %r failed" % word) 
			print("\t\tResult should have been %r"% palindromeTest[word])
			print("\t\tYour answer was %r\n" % palindrome(word))

	#Encryption testing
	print("\nTesting the encryption function")

	encryptionTest = {"civic":"djwjd","banana":"cbobob","button":"cvuupo","red rum, sir, is murder!":"sfe!svn-!tjs-!jt!nvsefs\"","was it a cat i saw?":"xbt!ju!b!dbu!j!tbx@"}

	for message in encryptionTest:
		if encryptionTest[message] == encrypt(message):
			print("\tTest %r passed" % message)
		else:
			print("\tTest %r failed" % message)
			print("\t\tResult should have been %r" % encryptionTest[message])
			print("\t\tYour answer was %r\n" % encrypt(message))

	print("\nTesting the decryption function")

	for message in encryptionTest:
		if message == decrypt(encryptionTest[message]):
			print("\tTest %r passed" % message)
		else:
			print("\tTest %r failed" % message)
			print("\t\tResult should have been %r" % encryptionTest[message])
			print("\t\tYour answer was %r\n" % encrypt(message))

