import RPi.GPIO as GPIO 
from motor_class import motor
import time

m = motor()

try:
	m.forward()
	time.sleep(1.5)
	m.backward()
	time.sleep(1.5)
	m.goRight()
	time.sleep(1.5)
	m.goLeft()
	time.sleep(1.5)
	m.turnLeft()
	time.sleep(1.5)
	m.turnRight()
	time.sleep(1.5)
	m.stop()
	time.sleep(1.5)

finally:
	m.stop()
	GPIO.cleanup()

