import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)




class motor:

	def __init__(self):
	
		GPIO.setwarnings(False)
	
		self.LF = 9
		self.LB = 7
		self.RF = 8
		self.RB = 10

		GPIO.setup(self.LF, GPIO.OUT)
		GPIO.setup(self.LB, GPIO.OUT)
		GPIO.setup(self.RF, GPIO.OUT)
		GPIO.setup(self.RB, GPIO.OUT)

	def forward(self, seconds):
		print("moving forwards for %i" %seconds)
		GPIO.output(self.LF, True)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, True)
		GPIO.output(self.RB, False)
		time.sleep(seconds)

	def backward(self, seconds):
		print("moving backwards for %i" %seconds)
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, True)
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, True)
		time.sleep(seconds)

	def turnRight(self, seconds):
		print("turning right for %i" %seconds)
		GPIO.output(self.LF, True)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, True)
		time.sleep(seconds)	

	def turnLeft(self, seconds):
		print("turning left for %i" %seconds)
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, True)
		GPIO.output(self.RF, True)
		GPIO.output(self.RB, False)
		time.sleep(seconds)

	def goRight(self, seconds):
		print("going right for %i" %seconds)
		GPIO.output(self.LF, True)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, False)
		time.sleep(seconds)	

	def goLeft(self, seconds):
		print("going left for %i" %seconds)
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, True)
		GPIO.output(self.RB, False)
		time.sleep(seconds)

	def stop(self):
		print("stopping")
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, False)