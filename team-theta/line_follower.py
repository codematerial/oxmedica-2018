import RPI.GPIO as GPIO
from motor_class import motor
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
light_sensor_pin= 25

GPIO.setup(light_sensor_pin,GPIO.IN) 


try:

	while True:

		if GPIO.input(light_sensor_pin): 
			motor.forward()
		else:
			motor.turnRight()

# That's more like it! Don't forget the finally block with a GPIO cleanup command - Sam