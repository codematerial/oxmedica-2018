#############################################################
# Dojo Instructions
#############################################################

"""
Below are some unfinished functions.
To test if your functions work, simply run this file.

palindrome(sentence)

	This function should take one string argument
	It should return True or False if the string is the same forwards as it is backwards
	It should ignore punctuation and spaces


encrypt(message)

	This function should take one string argument
	It should return a string where each character is shifted by one. a->b, b->c, x->y, etc
	For example encrypt("banana") should return "cbobob"


decrypt(message)

	This should decrypt the encrypted message returned by encrypt
	For example decrypt("cbobob") should return "banana"


IMPORTANT TIPS

	message[::-1] will return the string message reversed
	a string is just a list of characters
	ord(char) will return the character char as a number
	chr(num) will return the integer num as a character
"""

#############################################################
# Functions
#############################################################



def palindrome(sentence):
	if (sentence)==(sentence)==[::-1]:
		return True
	else:
		return False
###
# in here i tried to make a sentence to be reversed but i think i'm a looser :(
# You tried so you're not - Sam
###	
def encrypt(message):
#	message.replace("a","b")
#	message.replace("b","c")
#	message.replace("c","d")
#	message.replace("d","e")
#	message.replace("e","f")
#	message.replace("f","g")
#	message.replace("g","h")
#	message.replace("i","j")
#	message.replace("j","k")
#	message.replace("k","l")
#	message.replace("l","m")
#	message.replace("m","n")
#	message.replace("n","o")
#	message.replace("o","p")
#	message.replace("p","q")
#	message.replace("q","r")
#	message.replace("r","s")
#	message.replace("s","t")
#	message.replace("t","u")
#	message.replace("u","v")
#	message.replace("v","w")
#	message.replace("x","y")
#	message.replace("y","z")
#	message.replace("z","a")

#############################
#in here i've changed the alphabit with a new one which we're using for the Encryption still a loser :(
#Still, not a looser - Sam
###########################
def decrypt(message):
	return None





#############################################################
# Testing code, do not edit
#############################################################

if __name__ == "__main__":

	#Palindrome testing
	print("Testing the palindrome function")

	palindromeTest = {"anna":True,"civic":True,"banana":False,"button":False,"red rum, sir, is murder!":True,"was it a cat i saw?":True}
	
	for word in palindromeTest:
		if palindrome(word) == palindromeTest[word]:
			print("\tTest %r passed" % word)
		else:
			print("\tTest %r failed" % word) 
			print("\t\tResult should have been %r"% palindromeTest[word])
			print("\t\tYour answer was %r\n" % palindrome(word))

	#Encryption testing
	print("\nTesting the encryption function")

	encryptionTest = {"civic":"djwjd","banana":"cbobob","button":"cvuupo","red rum, sir, is murder!":"sfe!svn-!tjs-!jt!nvsefs\"","was it a cat i saw?":"xbt!ju!b!dbu!j!tbx@"}

	for message in encryptionTest:
		if encryptionTest[message] == encrypt(message):
			print("\tTest %r passed" % message)
		else:
			print("\tTest %r failed" % message)
			print("\t\tResult should have been %r" % encryptionTest[message])
			print("\t\tYour answer was %r\n" % encrypt(message))

	print("\nTesting the decryption function")

	for message in encryptionTest:
		if message == decrypt(encryptionTest[message]):
			print("\tTest %r passed" % message)
		else:
			print("\tTest %r failed" % message)
			print("\t\tResult should have been %r" % encryptionTest[message])
			print("\t\tYour answer was %r\n" % encrypt(message))

