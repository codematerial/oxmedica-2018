from motor_class import motor as oldMotor
from improved_motor_class import motor as newMotor
import time
import RPi.GPIO as G

om = oldMotor()
nm = newMotor()

lsp = 25

G.setwarnings(False)
G.setmode(G.BCM)
G.setup(lsp, G.IN)






try:
	while True:

		om.forward()

		if G.input(lsp):
			pass
		else:
			nm.backward(1)
			nm.goRight(1.5)
	
finally:
	nm.stop()
	G.cleanup()

