import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)



class motor:

	def __init__(self):

		GPIO.setwarnings(False)
	
		self.LF = 9
		self.LB = 7
		self.RF = 8
		self.RB = 10

		GPIO.setup(self.LF, GPIO.OUT)
		GPIO.setup(self.LB, GPIO.OUT)
		GPIO.setup(self.RF, GPIO.OUT)
		GPIO.setup(self.RB, GPIO.OUT)

	def forward(self):
		print("moving forwards")
		GPIO.output(self.LF, True)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, True)
		GPIO.output(self.RB, False)

	def backward(self):
		print("moving backwards")
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, True)
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, True)

	def turnRight(self):
		print("turning right")
		GPIO.output(self.LF, True)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, True)	

	def turnLeft(self):
		print("turning left")
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, True)
		GPIO.output(self.RF, True)
		GPIO.output(self.RB, False)

	def goRight(self):
		print("going right")
		GPIO.output(self.LF, True)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, False)	

	def goLeft(self):
		print("going left")
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, True)
		GPIO.output(self.RB, False)

	def stop(self):
		print("stopping")
		GPIO.output(self.LF, False)
		GPIO.output(self.LB, False)
		GPIO.output(self.RF, False)
		GPIO.output(self.RB, False)

# Perfect! - Sam