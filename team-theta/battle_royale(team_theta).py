from motor_class import motor as oldMotor
from improved_motor_class import motor as newMotor
import time
import RPi.GPIO as G

om = oldMotor()
nm = newMotor()

lsp = 25

G.setwarnings(False)
G.setmode(G.BCM)
G.setup(lsp, G.IN)






try:
	while True:
		while G.input(lsp):
			print("ramming speed!")
			om.forward()
		while not G.input(lsp):
			print("nope")
			om.backward()
			om.turnRight()
finally:
	nm.stop()
	G.cleanup()

