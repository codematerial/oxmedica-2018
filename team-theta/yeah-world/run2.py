import numpy
from tensorflow import keras
from camera import Camera
from pinet import PiNet


extractor = PiNet()

classifier = keras.models.load_model("net2.h5")

camera = Camera(training_mode=False)

while True:

    frame = camera.next_frame()
    
    features = extractor.features(frame)

    classes = classifier.predict(numpy.array([features]))[0]

    obstacle = classes[0]

    not_obstacle = classes[1]

if obstacle < 0.79:
	print("there is nothing")

else: 
	print("i see")

	
