import RPi.GPIO as G
from motor_class import motor
import time

M = motor()

G.setwarnings(False)
G.setmode(G.BCM)

lsp = 25

G.setup(lsp, G.IN)
try:
	while True:
			while G.input(lsp):
 				print("Oops, I have to go left")
 				M.goLeft()
 			while not G.input(lsp):
 				print("the sensor see a black surface")
 				M.forward()
 			while G.input(lsp):
 				print("Oops, I have to go right")
 				M.goRight()
 			while not G.input(lsp):
 				print("the sensor is seeing a black surface")
 				M.forward()
		
finally:
	G.cleanup() 

#Perfect! - Sam