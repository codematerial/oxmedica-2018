import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

l_motor_forwards_pin = 7
l_motor_backwards_pin = 8
r_motor_forwards_pin = 9
r_motor_backwards_pin = 10

GPIO.setup(7, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(9, GPIO.OUT)
GPIO.setup(10, GPIO.OUT)

GPIO.output(l_motor_forwards_pin, False)
GPIO.output(l_motor_backwards_pin, False)
GPIO.output(r_motor_forwards_pin, False)
GPIO.output(r_motor_backwards_pin, False)

GPIO.output(l_motor_forwards_pin, True)
GPIO.output(r_motor_forwards_pin, True)

time.sleep(1)

GPIO.cleanup()