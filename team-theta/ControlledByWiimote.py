from motor_class import motor
import cwiid
import time
import RPi.GPIO as GPIO

m = motor()
button_delay = 0.1

print 'Please press buttons 1 + 2 on your Wiimote now ...'
time.sleep(1)

try:
  wii=cwiid.Wiimote()
except RuntimeError:
  print "Cannot connect to your Wiimote. Run again and make sure you are holding buttons 1 + 2!"
  quit()

print 'Wiimote connection established!\n'
print 'Press PLUS and MINUS together to disconnect and quit.\n'

time.sleep(3)

wii.rpt_mode = cwiid.RPT_BTN

while True:

  buttons = wii.state['buttons']

  if (buttons - cwiid.BTN_PLUS - cwiid.BTN_MINUS == 0):
    print '\nClosing connection ...'
  
    wii.rumble = 1
    time.sleep(1)
    wii.rumble = 0
    exit(wii)

  if (buttons & cwiid.BTN_LEFT):
    m.goLeft()
    time.sleep(button_delay)

  if(buttons & cwiid.BTN_RIGHT):
    m.goRight()
    time.sleep(button_delay)

  if (buttons & cwiid.BTN_UP):
    m.forward()
    time.sleep(button_delay)

  if (buttons & cwiid.BTN_DOWN):
    m.backward()
    time.sleep(button_delay)

  if (buttons & cwiid.BTN_1):
    time.sleep(button_delay)

  if (buttons & cwiid.BTN_2):
    time.sleep(button_delay)

  if (buttons & cwiid.BTN_A):
    time.sleep(button_delay)

  if (buttons & cwiid.BTN_B):
    m.stop()
    time.sleep(button_delay)

  if (buttons & cwiid.BTN_MINUS):
    time.sleep(button_delay)

  if (buttons & cwiid.BTN_PLUS):
    time.sleep(button_delay)

  if not buttons:
    m.stop()

  



GPIO.cleanup()

# Coded by The Raspberry Pi Guy. Work based on some of Matt Hawkins's!