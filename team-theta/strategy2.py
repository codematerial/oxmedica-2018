from motor_class import motor as oldMotor
from improved_motor_class import motor as newMotor
import time
import RPi.GPIO as G

om = oldMotor()
nm = newMotor()

front_lsp = 25
back_lsp = 24

G.setup(front_lsp, G.IN)
G.setup(back_lsp, G.IN)



G.setwarnings(False)
G.setmode(G.BCM)

try:
	while True:
		while G.input(front_lsp):
			print("ramming speed!")
			nm.forward(3)
		if not G.input(front_lsp):
			print("nope")
			nm.backward(0.8)
			nm.turnRight(2.5)
		while G.input(back_lsp):
			pass
		if not G.input(back_lsp):
			print("plz no")
			om.goRight()
			om.goLeft()	
finally:
	nm.stop()
	G.cleanup()	









