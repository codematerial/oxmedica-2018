import cwiid
import time
from engine import engine
from subprocess import call

import numpy
from tensorflow import keras
from camera import Camera
from pinet import PiNet


extractor = PiNet()

classifier = keras.models.load_model("thescaredrobot.h5")

camera = Camera(training_mode=False)

running = True

print("Press 1 + 2 on your Wii Remote now ...")

time.sleep(1)

E = engine()

wii = cwiid.Wiimote()

wii.rpt_mode = cwiid.RPT_BTN


while running:

	frame = camera.next_frame()

	features = extractor.features(frame)

	classes = classifier.predict(numpy.array([features]))[0]
	
	buttons = wii.state['buttons']

	print(classes)

	if classes[0] > 0.6:

		E.right(0.5)
		E.backwards(0.2)

	elif buttons & cwiid.BTN_RIGHT:
		print("right button has been pressed")
		E.right(0)

	elif buttons & cwiid.BTN_UP:
		print("up button has been pressed")
		E.forward(0)

	elif buttons & cwiid.BTN_DOWN:
		print("down button has been pressed")
		E.backwards(0)

	elif buttons & cwiid.BTN_LEFT:
		print("left button has been pressed")
		E.left(0) 
	elif buttons & cwiid.BTN_A & cwiid.BTN_B:
		print("raspberry pi is shutting down")
		call("sudo shutdown -h now", shell=True)

	elif buttons & cwiid.BTN_A:
		print("A button has been pressed, so change to black eyes")

	else:
		E.stop()








