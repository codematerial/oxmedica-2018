import RPi.GPIO as GPIO
import time

class engine:

	def __init__(self):

		GPIO.setmode(GPIO.BCM)
		
		self.leftengineforward = 8

		self.leftenginebackwards = 7
		
		self.rightengineforward = 10

		self.rightenginebackwards = 9

		GPIO.setup(self.leftengineforward,GPIO.OUT)
	
		GPIO.setup(self.leftenginebackwards,GPIO.OUT)
		
		GPIO.setup(self.rightengineforward,GPIO.OUT)
		
		GPIO.setup(self.rightenginebackwards,GPIO.OUT)

	def forward(self,movementtime):

		GPIO.output(self.leftengineforward,True)

		GPIO.output(self.leftenginebackwards,False)

		GPIO.output(self.rightengineforward,True)

		GPIO.output(self.rightenginebackwards,False)

		time.sleep(movementtime)

	def backwards(self,movementtime):

		GPIO.output(self.leftengineforward,False)

		GPIO.output(self.leftenginebackwards,True)

		GPIO.output(self.rightengineforward,False)

		GPIO.output(self.rightenginebackwards,True)

		time.sleep(movementtime)
	
	def right(self, movementtime):

		GPIO.output(self.leftengineforward, True)

		GPIO.output(self.leftenginebackwards, False)

		GPIO.output(self.rightengineforward,False)

		GPIO.output(self.rightenginebackwards, False)

		time.sleep(movementtime)

	def left(self, movementtime):

		GPIO.output(self.leftengineforward, False)

		GPIO.output(self.leftenginebackwards, False)

		GPIO.output(self.rightengineforward, True)

		GPIO.output(self.rightenginebackwards, False)

		time.sleep(movementtime)
	
	def stop(self):
		GPIO.output(self.leftengineforward, False)

		GPIO.output(self.leftenginebackwards, False)

		GPIO.output(self.rightengineforward, False)

		GPIO.output(self.rightenginebackwards, False)

	def cleanup(self):
		GPIO.claenup()

# Nice cleanup function, keep in mind you don't need to space everything so much! - Sam