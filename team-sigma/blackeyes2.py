import RPi.GPIO as GPIO
from engine import engine  

GPIO.setmode(GPIO.BCM)

GPIO.setwarnings(False)

E = engine()

line_sensor_pin = 25

GPIO.setup(line_sensor_pin, GPIO.IN)

while True:
		
	while not GPIO.input(line_sensor_pin):
		E.left(0)

	while GPIO.input(line_sensor_pin):
		E.right(0)
	
	while not GPIO.input(line_sensor_pin):
		E.right(0)

	while GPIO.input(line_sensor_pin):
		E.left(0)