class waterbottle:

	def __init__(self):

		self.weight = 500
		self.lidOn = True

	def drink(self, time):
		if not self.lidOn:
			print("drinking some water")
			self.weight=self.weight- time*10

	def removeLid(self):
		print("lid has been removed!")
		self.lidOn = False

	def addLid(self):
		print("lid has been added")
		self.lidOn = True

