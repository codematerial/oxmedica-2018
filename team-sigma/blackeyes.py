import RPi.GPIO as GPIO
from engine import engine  

GPIO.setmode(GPIO.BCM)

GPIO.setwarnings(False)

E = engine()

line_sensor_pin = 25

GPIO.setup(line_sensor_pin, GPIO.IN)
def rightleft():
	E.left(0.3)
	E.right(0.3)
	
def walking():
	
	while True:
		
		E.left(0.1)
		
		if  GPIO.input(line_sensor_pin):
			
			E.stop()

		elif not GPIO.input(line_sensor_pin):

			E.right(0.1)

while True:

	if not GPIO.input(line_sensor_pin):
		rightleft()
