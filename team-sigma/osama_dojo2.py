def palindrome(sentence):
	sentence = sentence.replace(",","").replace("!","").replace("?","").replace("'","")
	if (sentence) == (sentence)[::-1]:
		return True
	else:
		return False

	#Nicely done! - Sam

def encrypt(message):
	message = message.replace("a","b").replace("b","c").replace("c","d").replace("d","e").replace("e","f").replace("f","g").replace("g","h").replace("h","i").replace("n","o")	
	return message 
	#Did this the long way round but nicely done regardless - Sam
def decrypt(message):
	return None





#############################################################
# Testing code, do not edit
#############################################################

if __name__ == "__main__":

	#Palindrome testing
	print("Testing the palindrome function")

	palindromeTest = {"anna":True,"civic":True,"banana":False,"button":False,"red rum, sir, is murder!":True,"was it a cat i saw?":True}
	
	for word in palindromeTest:
		if palindrome(word) == palindromeTest[word]:
			print("\tTest %r passed" % word)
		else:
			print("\tTest %r failed" % word) 
			print("\t\tResult should have been %r"% palindromeTest[word])
			print("\t\tYour answer was %r\n" % palindrome(word))

	#Encryption testing
	print("\nTesting the encryption function")

	encryptionTest = {"civic":"djwjd","banana":"cbobob","button":"cvuupo","red rum, sir, is murder!":"sfe!svn-!tjs-!jt!nvsefs\"","was it a cat i saw?":"xbt!ju!b!dbu!j!tbx@"}

	for message in encryptionTest:
		if encryptionTest[message] == encrypt(message):
			print("\tTest %r passed" % message)
		else:
			print("\tTest %r failed" % message)
			print("\t\tResult should have been %r" % encryptionTest[message])
			print("\t\tYour answer was %r\n" % encrypt(message))

	print("\nTesting the decryption function")

	for message in encryptionTest:
		if message == decrypt(encryptionTest[message]):
			print("\tTest %r passed" % message)
		else:
			print("\tTest %r failed" % message)
			print("\t\tResult should have been %r" % encryptionTest[message])
			print("\t\tYour answer was %r\n" % encrypt(message))