import cwiid
import time
from engine import engine
from subprocess import call #line 4 and line 42 https://www.raspberrypi.org/forums/viewtopic.php?t=133665

print("Press 1 + 2 on your Wii Remote now ...")

time.sleep(1)

E = engine()

wii = cwiid.Wiimote()

wii.rpt_mode = cwiid.RPT_BTN

while True:

	buttons = wii.state['buttons']

	if buttons & cwiid.BTN_LEFT:
		print("left button has been pressed")
		E.left(0)

	elif buttons & cwiid.BTN_RIGHT:
		print("right button has been pressed")
		E.right(0)

	elif buttons & cwiid.BTN_UP:
		print("up button has been pressed")
		E.forward(0)

	elif buttons & cwiid.BTN_DOWN:
		print("down button has been pressed")
		E.backwards(0)

	elif buttons & cwiid.BTN_A:
		print("A button has been pressed, so change to black eyes")

	elif buttons & cwiid.BTN_1 & cwiid.BTN_2:
		print("raspberry pi is shutting down")
		call("sudo shutdown -h now", shell=True)

	else:
		E.stop()
	

# Perfect! - Sam