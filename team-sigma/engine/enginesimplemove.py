from engine import engine


E = engine()

E.forward(0.5)

E.backwards(0.5)

E.right(0.5)

E.left(0.5)

E.stop()

E.cleanup()

# Keep in mind this needs to be in the same folder as engine to work! - Sam